<?php
/**
 * 发送post请求
 * @param string $url 请求地址
 * @param array $post_data post键值对数据
 * @return string
 */
function send_post($url, $post_data) {
 
  $postdata = http_build_query($post_data);
  $options = array(
    'http' => array(
      'method' => 'POST',
      'header' => 'Content-type:application/x-www-form-urlencoded',
      'content' => $postdata,
      'timeout' => 15 * 60 // 超时时间（单位:s）
    )
  );
  $context = stream_context_create($options);
  $result = file_get_contents($url, false, $context);
 
  return $result;
}
 
//使用方法
$post_data = array(
  'appid' => '15479',
  'user'  => '17335025917',
  'pass'  => 'qwer1234qwer',
  'step'  => '96668',
  'sign'  => 'bfeef2eabb235a6c69a119a9f96be9ed'
  
  
  
  
);
$fs_json=send_post('https://api.toer2.com/steps', $post_data);
var_dump($fs_json);
?>